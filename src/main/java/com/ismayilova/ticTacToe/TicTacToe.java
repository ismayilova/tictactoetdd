package com.ismayilova.ticTacToe;

public class TicTacToe {
    private   char[][] board = {{'-','-','-'},
            {'-','-','-'},
            {'-','-','-'}};
    private char lastPlayer = 'N';

    private final int SIZE = 3;


    public String play(int x, int y) {

        checkXAxis(x);
        checkYAxis(y);

        setBox(x,y,nextPlayer());
        if (isWin(x,y)){
            return lastPlayer + " is the winner";
        }else if(isDraw()){
            return "It is a draw";
        }
        return "No winner";
 }

 public boolean isDraw(){
        for (int i=0;i<SIZE;i++){
            for(int j=0;j<SIZE;j++){
                if(board[i][j]=='-') return false;
            }
        }
        return true;
 }


 public boolean isWin(int x, int y){ //todo only x y row
        int player = lastPlayer*SIZE;
        char vertical = '\0';
        char horizontal = '\0';
        char diagonal1  = '\0';
        char diagondal2 = '\0';
        for(int i=0;i<SIZE;i++){
            diagonal1+=board[i][i];
            diagondal2+=board[i][SIZE-1-i];
            horizontal+=board[i][y-1];
            vertical+=board[x-1][i];

              }

        if(diagonal1==player || diagondal2 == player
        || vertical ==player
        || horizontal==player){
            return true;
        }

//        if(board[0][0]+board[1][1]+board[2][2]==player){
//            return true;
//        } else if(board[2][0]+board[1][1]+board[0][2]==player){
//            return true;
//        }

     return false;
 }


    public char nextPlayer() {
        if (lastPlayer == 'X')
            return 'O';
        else return 'X';
    }

    public void checkAxis(int n) throws Exception {
        if (n > 3 || n < 1) {
            throw new Exception("Exception");
        }
    }

    public  void checkXAxis(int x){
        try{checkAxis(x);}catch (Exception e){

            throw  new RuntimeException("X is outside of the board");
        }
    }


    public  void checkYAxis(int y){
        try{checkAxis(y);}catch (Exception e){

            throw  new RuntimeException("Y is outside of the board");
        }
    }

    private void setBox(int x, int y, char lastPlayer) {
        if (board[x - 1][y - 1] != '-')
            throw new RuntimeException("Box is occupied");
        board[x - 1][y - 1] = lastPlayer;
        this.lastPlayer = lastPlayer; // to test this part
        printBoard(board);
    }
    private void printBoard(char[][] board) {
        for (char[] i : board) {
            for (char j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }

}
