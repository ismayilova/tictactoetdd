package com.ismayilova.ticTacToe;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

//import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
//import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.*;

public class TicTacToeSpec {

    private TicTacToe ticTacToe;


    @Test
    public void isOdd(){
        int n= 4;
        assertThat(n%2).isEqualTo(0);
    }


    @BeforeEach
    public final void before(){
        ticTacToe = new TicTacToe();
    }


    @Test
   public    void whenXOutOfBoardThenRuntimeException(){
        assertThatThrownBy(()->ticTacToe.play(5,2))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("X is outside of the board");

    }

    @Test
    public  void whenYOutOfBoardThenThenRuntimeException(){
        assertThatThrownBy(()->ticTacToe.play(2,5))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Y is outside of the board");
    }




    @Test
    public void whenOccupiedThenRuntimeException(){
        ticTacToe.play(1,1);

        assertThatThrownBy(()->ticTacToe.play(1,1))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Box is occupied");

    }

    @Test
    public void givenFirstTurnWhenNextPlayerThenX(){
         assertThat(ticTacToe.nextPlayer()).isEqualTo('X');
    }

    @Test
    public void givenLastTurnWasXWhenNextPlayerThenO(){
        ticTacToe.play(1,1);

        assertThat(ticTacToe.nextPlayer()).isEqualTo('O');
    }

    @Test
    public void whenPlayThenNoWinner(){
         String actual = ticTacToe.play(1,1);
         assertThat(actual).isEqualTo("No winner");
    }

    @Test
    public void whenPlayAndWholeVerticalLineWhenWinner(){//TODO  IS there is an error - cos it is vertical line???
        ticTacToe.play(1,1);//x
        ticTacToe.play(1,2); //O
        ticTacToe.play(2,1); //X
        ticTacToe.play(2,2);//o
        String result  = ticTacToe.play(3,1); // X
        assertThat(result).isEqualTo("X is the winner");

    }

    @Test
    public void whenPlayAndWholeHorizontalLineWhenWinner(){
        ticTacToe.play(1,1);//X
        ticTacToe.play(2,1);//O
        ticTacToe.play(3,2);//X
        ticTacToe.play(2,2);//O
        ticTacToe.play(1,3);//X
        String result = ticTacToe.play(2,3);//O
        assertThat(result).isEqualTo("O is the winner");

    }

    @Test
    public  void whenPlayAndBottomTopDiagonalLineThenWinner(){
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(1, 2); // O
        String result = ticTacToe.play(3,1);
        assertThat(result).isEqualTo("X is the winner");

    }


    @Test
    public void whenAllBoxesAreFilledThenDraw(){
        ticTacToe.play(1, 1);
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 3);
        ticTacToe.play(2, 1);
        ticTacToe.play(2, 3);
        ticTacToe.play(2, 2);
        ticTacToe.play(3, 1);
        ticTacToe.play(3, 3);
        String result = ticTacToe.play(3, 2);
        assertThat(result).isEqualTo("It is a draw");
    }


}
